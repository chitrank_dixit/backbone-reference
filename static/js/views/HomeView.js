HomeView = Backbone.View.extend({

    events: {  },

    template: Handlebars.getTemplate('home'),

    initialize: function() {
        _.bindAll(this, 'render');
        this.collection.on('reset', this.render);
    },

    render: function(eventName) {

        this.$el.html(this.template({
            items: this.collection.toJSON()
        }));

        return this;
    }

});