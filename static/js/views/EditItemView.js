EditItemView = FormView.extend({

    events: {
        'click .btn-primary' : 'saveItem',
        'blur input': 'updateModel',
        'click input[type="checkbox"]' : 'updateModel'
    },

    template: Handlebars.getTemplate('editItem'),

    initialize: function() {
        // https://github.com/thedersen/backbone.validation#configuration
        Backbone.Validation.bind(this, {
            valid: this.clearInputError,
            invalid: this.showInputError
        });
        _.bindAll(this, 'render');
    },

    render: function(eventName) {
        this.$el.html(this.template(this.model.toJSON()));

        this.model.on('validated:valid', this.valid, this);
        this.model.on('validated:invalid', this.invalid, this);

        // subview for file list
        this.itemFileCollection = new FileCollection(null, { itemId: this.model.id });
        this.fileListView = new FileListView({
            el: this.$('.fileList'),
            collection: this.itemFileCollection
        });

        // Only fetch if the model exists already
        if (this.model.id) {
            this.itemFileCollection.fetch();
        } else {
            this.fileListView.render();
        }


        var self = this;

        // This isn't backboney, but it seems to be the status quo
        this.$('#file').fileupload({
            url: Config.get('api') + '/files',
            dataType: 'json',
            xhrFields: {
                withCredentials: true
            },
            done: function(e, data) {
                self.$('#file').val('');
                // add new files to the collection, which should rerender our list of files
                $.each(data.result.files, function(idx, item) {
                    self.itemFileCollection.add(item);
                    self.model.addFile(item.id);
                });
            },
            fail: function(e, data) {
                Session.error("Could not upload the file: " + data.errorThrown);
            }
        });

        return this;
    },

    valid: function(){
        this.$('.btn-primary').removeAttr('disabled');
    },

    invalid: function(){
        this.$('.btn-primary').attr('disabled', 'disabled');
    },

    saveItem: function() {
        // maybe check for files here?
        this.model.save(null, {
            success: function(model, response, options) {
                application.navigate('#/');
            },
            error: function(model, xhr, options) {
                Session.error("Save failed");
            },
            patch: true
        });
    }

//    change: function(event) {
//        // remove alerts
//
//        // apply change to model
//        var target = event.target;
//        var change = {};
//        change[target.name] = target.value;
//        this.model.set(change);
//
//        // run validation on changed field
//
//    },
//
//    beforeSave: function() {
//        var self = this;
//        // validation stuff, etc.
//        self.saveItem();
//    },
//
//    saveItem: function() {
//        var self = this;
//        this.model.save(null, {
//            success: function(model) {
//
//            },
//            error: function() {
//                Session.error("An error occurred saving this item.");
//            }
//        })
//    }

});