SignUpView = Backbone.View.extend({

    events: { 'submit form' : 'createUser' },

    template: Handlebars.getTemplate('signUp'),

    initialize: function() { },

    render: function(eventName) {
        $(this.el).html(this.template());

        // these were in initialize, but they don't exist at init time
        this.form = this.$('form');
        this.firstName = this.$('input[name=firstName]');
        this.lastName = this.$('input[name=lastName]');
        this.emailField = this.$('input[name=email]');
        this.passwordField = this.$('input[name=password1]');
        this.passwordConfirmField = this.$('input[name=password2]');
        this.submitButton = this.$('button');

        return this;
    },

    attributes: function() {
        return {
            first_name: (this.firstName ? this.firstName.val() : null),
            last_name: (this.lastName ? this.lastName.val() : null),
            email: (this.emailField ? this.emailField.val() : null),
            password: (this.passwordField ? this.passwordField.val() : null)
        };
    },

    createUser: function() {
        if (this.submitButton.hasClass('disabled') && this.form.data('user-created') !== true) {
            return false;
        } else {
            this.submitButton.addClass('disabled');
        }

        try {
            var self = this,
                user = new User(this.attributes());

            user.save(null, {
                error: function(originalModel, response, options) {
                    self.$('input').removeClass('error');
                    // TODO: form validation & display server side error messages
                    var errors = JSON.parse(response.responseText).errors;
                    _.each(errors, function(value, key) {
                        self.$('input[name='+key+']').addClass('error');
                    });
                    self.submitButton.removeClass('disabled');
                },
                success: function() {
                    self.form.data('user-created', true);
                    application.navigate('#/');
                }
            });
        } catch (exception) {
            window.console && console.log('Exception saving new user: ' + exception.message);
        }

        return (this.form.data('user-created') === true);
    }
});