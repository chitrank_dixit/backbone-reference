FileListView = Backbone.View.extend({
    events: { },

    template: Handlebars.getTemplate("fileList"),

    initialize: function(options) {
        _.bindAll(this, 'render');
        this.collection.on('reset add', this.render);
    },

    render: function(eventName) {
        $(this.el).html(this.template({
            files: this.collection.toJSON()
        }));

        return this;
    }
});