NavigationView = Backbone.View.extend({

    events: { 'click .logout' : 'logout' },

    template: Handlebars.getTemplate('navigation'),

    model: Session.getCurrentUser(),

    initialize: function() {
        _.bindAll(this, 'render');
        this.model.on('change', this.render);
    },

    render: function(eventName) {
        window.console && console.log('Rendering navigation: ' + this.model.id );
        $(this.el).html(this.template({ user: this.model.toJSON() }));
        return this;
    },

    logout: function() {

        var self = this;
        // should be as easy as this, yes?
        Session.logout();

    }

});