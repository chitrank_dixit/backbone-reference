FormView = Backbone.View.extend({
    clearInputError: function(view, attr) {
        var input = view.$('[name="' + attr + '"]');
        input.closest('.control-group').removeClass('error');
        input.siblings(".help-inline").remove();
    },

    showInputError: function(view, attr, error) {
        var input = view.$('[name="' + attr + '"]');
        input.closest('.control-group').addClass('error');
        input.after('<span class="help-inline">'+error+'</span>');
    },

    updateModel: function(el){
        var $el = $(el.target);
        if ($el[0].type == 'checkbox') {
            // get all checked items with this name and set their values at once
            var checked = this.$('input[name="'+$el.attr('name')+'"]:checked').map(function () {
                return this.value;
            }).get();
            this.model.set($el.attr('name'), checked);
        } else if ($el[0].type != 'file') {
            this.model.set($el.attr('name'), $el.val());
        }
    }
});