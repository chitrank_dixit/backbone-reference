ItemCollection = Backbone.Collection.extend({
    model: Item,
    url: function() {
        return Config.get('api') + '/user/items';
    },
    parse: function(response) {
        return response.items;
    }
});