Item = Backbone.Model.extend({
    urlRoot: Config.get('api') + '/user/item',
    validation: {
        description: {
            required: true
        }
    },
    addFile: function(fileId) {
        var files = this.get('files');
        if (files === undefined) {
            files = [];
        }
        files.push(fileId);
        this.set('files', files);
    }
});