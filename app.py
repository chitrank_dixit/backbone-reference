import os
import random
import shutil
import string
import sys

from flask import Flask, jsonify, render_template, request, send_file
from flask.ext.login import LoginManager, login_required, login_user,\
                            logout_user, current_user
from flask.ext.uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES,\
                              TEXT, UploadNotAllowed
from passlib.context import CryptContext
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from werkzeug import secure_filename

from models import User, Item, File, ItemFile, initialize_db, transaction


# =============================================================================
#
# Configuration
#
# =============================================================================


# The directory to which all of the data for this reference application should
# be saved to. I.e. all of the uploaded files, the SQLite database, etc.
APP_DIR = '/var/tmp/backbone-reference/'


# -------- THERE SHOULD BE NO NEED TO MODIFY ANYTHING BELOW HERE --------------

# =============================================================================
#
# Flask App
#
# =============================================================================

app = Flask(__name__)

# Set the secret key used to cryptographically sign session cookies.
# This was generated as follows:
#   import os
#   os.random(24)
app.secret_key = 'L\xfe\x9a7\xa5\xa5\xa2\xe8\xc5\x83\xcf\x18j\xc0\xaa\xfd\n\xdc\x87\x94\xf8\x0f\xe0\n'


# =============================================================================
#
# Flask App extensions to be setup
#
# =============================================================================

# Setup Flask-Login. Checkout the full documentation here:
#   http://packages.python.org/Flask-Login/
login_manager = LoginManager()
login_manager.setup_app(app)

# Setup Flask-Uploads so that we can easily allow uploading of files. Checkout
# the full documentation here:
#   http://packages.python.org/Flask-Uploads/
app.config['UPLOADED_FILES_DEST'] = os.path.join('{}/uploads'.format(APP_DIR),
                                                'files')
user_uploaded_files = UploadSet('files', DOCUMENTS + IMAGES + TEXT + ('pdf', ))
configure_uploads(app, (user_uploaded_files))


# =============================================================================
#
# Other things to setup
#
# =============================================================================

# Setup the SQLAlchemy database engine and database session. For more details,
# chedkout the full documentation at:
#   http://www.sqlalchemy.org/
engine = create_engine('sqlite:///{}/bbref.db'.format(APP_DIR),
                        convert_unicode=True, echo=False)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False,
                                         bind=engine))

# Setup the passlib cryptographic context for password hashing. For more
# details, check out the passlib reference at:
#   http://packages.python.org/passlib/index.html
pwd_crypt_context = CryptContext(['sha256_crypt', 'ldap_salted_md5'])


# =============================================================================
#
# Flask App functions
#
# =============================================================================

@app.errorhandler(400)
def client_error(e):
    """Error handler for a 400 response."""
    return jsonify(dict(errors=dict(message="Client error."))), 400


@app.errorhandler(401)
def unauthorized(e):
    """Error handler for a 401 response."""
    return jsonify(dict(errors=dict(message="Unauthorized."))), 401


@app.errorhandler(404)
def does_not_exist(e):
    """Error handler for a 404 response."""
    return jsonify(dict(errors=dict(message="Does not exist."))), 404


@app.errorhandler(405)
def unsupported_method(e):
    """Error handler for a 405 response."""
    return jsonify(dict(errors=dict(message="Unsupported method."))), 405


@app.errorhandler(415)
def unsuported_media_type(e):
    """Error handler for a 415 response."""
    return jsonify(dict(errors=dict(message="Unsupported media type."))), 415


@app.errorhandler(500)
def server_error(e):
    """Error handler for a 500 response."""
    return jsonify(dict(errors=dict(message="Server error. " + e.message))), 500


@app.teardown_request
def remove_db_session(exception):
    """Remove the DB session at the teardown phase of the request."""
    db_session.remove()


@login_manager.user_loader
def load_user(id):
    """Load the user for the given id. Required by Flask-Login."""
    return db_session.query(User).filter_by(id=id).one()


# =============================================================================
#
# Flask App Routes
#
# =============================================================================

@app.route('/')
def index():
    """Serve back the index page.

    Since the entire index.html page works using Backbone.js and interactions
    with the REST API, this is pretty much the only route that actually returns
    HTML. The rest of the routes return JSON data that is consumed by this
    page.
    """
    return render_template('index.html')


@app.route('/login', methods=['POST'])
def login():
    """Allow a user to log in.

    The user must POST a JSON object of the form:
        {"email":"albert@einstein.com", "password":"pass"}
    which will then be validated. Upon successful validation the details of the
    logged in user are returned. If there is an error, some sort of error
    message is provided with the approprite HTTP status code.
    """
    if request.headers['Content-Type'] != 'application/json':
        return jsonify(dict(errors=dict(
                                    message="Unsupported media type."))), 415

    try:
        email = request.json['email']
    except KeyError:
        return jsonify(dict(errors=dict(
                                    message="No email provided."))), 400

    try:
        password = request.json['password']
    except KeyError:
        return jsonify(dict(errors=dict(
                                    message="No password provided."))), 400

    # Get the user from the database
    try:
        user = db_session.query(User).filter_by(email=email).one()
    except:
        return jsonify(dict(errors=dict(message="No matching user."))), 400

    # Make sure the passwords match
    if pwd_crypt_context.verify(password, user.password):
        login_user(user)
        return jsonify(user.to_dict()), 200
    else:
        return jsonify(dict(errors=dict(
                        message="Username and password do not match."))), 401


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    """Allow a user to log out."""
    logout_user()
    return jsonify(dict(message="Logged out."))


@app.route('/user', methods=['GET'])
@login_required
def profile():
    """Get the details of the current logged in user."""
    # Flask-Login provides us with the current session's user via the
    # aptly named current_user variable.
    return jsonify(current_user.to_dict())


@app.route('/user/items', methods=['GET', 'POST'])
@login_required
def user_items():
    """Get oa JSON object containing a list of items that the user has or
    create new ones.

    The POSTed JSON must be of the form:
        [{"description":"Some new item"}]
    If all goes well, the JSON data for the newly created item is returned.
    """
    if request.method == 'GET':
        return jsonify(dict(items=[item.to_dict() for item in current_user.items]))

    # Ok, we want to add a new set of items
    else:
        if request.headers['Content-Type'] != 'application/json':
            return jsonify(dict(errors=dict(
                                    message="Unsupported media type."))), 415

        if not isinstance(request.json, list):
            rj = [request.json]
        else:
            rj = request.json

        items = []
        for item_json in rj:
            item = Item(creator_id=current_user.id)
            try:
                item.description = item_json['description']
            except KeyError:
                return jsonify(dict(errors=dict(
                                    message="No description provided."))), 400
            if 'files' in item_json:
                for f_id in item_json['files']:
                    item.files.append(ItemFile(file_id=f_id))

            items.append(item)

        try:
            with transaction(db_session):
                for item in items:
                    db_session.add(item)
        except:
            return jsonify(dict(errors=dict(
                    message="Oops! There was an error adding the items."))), 500

        return jsonify(items.to_dict())

@app.route('/user/item', methods=['POST'])
@login_required
def user_item():
    if request.headers['Content-Type'] != 'application/json':
        return jsonify(dict(errors=dict(
                                message="Unsupported media type."))), 415

    item = Item(creator_id=current_user.id)
    try:
        item.description = request.json['description']
    except KeyError:
        return jsonify(dict(errors=dict(
                            message="No description provided."))), 400
    if 'files' in request.json:
        for f_id in request.json['files']:
            item.files.append(ItemFile(file_id=f_id))

    try:
        with transaction(db_session):
                db_session.add(item)
    except:
        return jsonify(dict(errors=dict(
                message="Oops! There was an error adding the item."))), 500

    return jsonify(item.to_dict())

@app.route('/user/item/<item_id>', methods=['GET', 'PATCH'])
@login_required
def user_item_by_id(item_id):
    """GET the specified item or PATCH it.

    If this is a GET requets, then it returns the details for the item.

    If this is a PATCH request, then the data is fist updated, and then the
    details are returned just like with the GET request.
    """
    if request.method == 'GET':
        try:
            item = db_session.query(Item).\
                                filter_by(id=item_id, creator_id=current_user.id).\
                                one()
        except:
            return jsonify(dict(errors=dict(message="No such item."))), 404
        return jsonify(item.to_dict())
    else:
        try:
            item = db_session.query(Item).\
                                filter_by(id=item_id, creator_id=current_user.id).\
                                one()
        except:
            return jsonify(dict(errors=dict(message="No such item."))), 404
        if request.method == 'GET':
            return jsonify(item.to_dict())
        else:
            if request.headers['Content-Type'] != 'application/json':
                return jsonify(dict(errors=dict(
                                        message="Unsupported media type."))), 415

            # Since we only have a description field for an item, that's all that
            # can be patched.
            with transaction(db_session):
                try:
                    item.description = request.json['description']
                except:
                    return jsonify(dict(errors=dict(
                                        message="No description to patch."))), 400
        return jsonify(item.to_dict())


@app.route('/user/item/<item_id>/files', methods=['GET', 'POST'])
@login_required
def user_item_files(item_id):
    """Get or add files to associate with an item.

    If this is a GET request, all of the files that are associated with the
    current item are returned.

    If this is a POST request, the submitted data must be of the form:
        [{"file_id": 1}, {"file_id": 2}]
    If the POST is successful, the result will be a JSON object of the same
    form as the GET request, but with the new files included.
    """
    try:
        item = db_session.query(Item).\
                            filter_by(id=item_id, creator_id=current_user.id).\
                            one()
    except:
        return jsonify(dict(errors=dict(message="No such item."))), 404

    if request.method == 'GET':
        try:
            return jsonify(dict(files=[f.to_dict() for f in item.files]))
        except:
            return jsonify(dict(errors=dict(message="No such item."))), 404
    else:
        if request.headers['Content-Type'] != 'application/json':
            return jsonify(dict(errors=dict(
                                    message="Unsupported media type."))), 415

        if not isinstance(request.json, list):
            rj = [request.json]
        else:
            rj = request.json

        # Create all of the necessary item to file associations and also check
        # to make sure that the current user actually owns the file that he is
        # trying to associate with the current item.
        files = []
        for file_json in rj:
            try:
                f = db_session.query(File).\
                                filter_by(id=file_json['file_id']).\
                                one()
            except:
                return jsonify(dict(errors=dict(message="No such file."))), 400

            if f.creator_id != current_user.id:
                return jsonify(dict(errors=dict(
                    message="Nice try, but that's not your're file!"))), 403

            files.append(ItemFile(item_id=item_id,
                                    file_id=file_json['file_id']))

        # Now add in the new associations
        try:
            with transaction(db_session):
                for f in files:
                    db_session.add(f)
        except:
            return jsonify(dict(errors=dict(
                    message="Oops! There was an error adding the item."))), 500

        return jsonify(dict(files=[f.to_dict() for f in item.files]))


@app.route('/user/files', methods=['GET'])
@login_required
def user_files():
    """Get all of the files that the user has."""
    return jsonify(dict(files=[f.to_dict() for f in current_user.files]))


@app.route('/user', methods=['POST'])
def create_user():
    """Create a new user.

    The provided JSON data for the new user must be of the form:
        {
            "first_name":"Thomas",
            "last_name":"Edison",
            "email":"thomas@edison.com",
            "password":"pass"
        }
    If all goes well, the data for the user will be returned as a JSON
    object.
    """
    if request.headers['Content-Type'] != 'application/json':
        return jsonify(dict(errors=dict(
                                    message="Unsupported media type."))), 415

    if not isinstance(request.json, list):
        rj = [request.json]
    else:
        rj = request.json

    users = []
    for user_json in rj:
        user = User()
        try:
            user.email = request.json['email']
        except KeyError:
            return jsonify(dict(errors=dict(
                                        message="No email provided."))), 400
        try:
            user.first_name = request.json['first_name']
        except KeyError:
            return jsonify(dict(errors=dict(
                                        message="No first_name provided."))), 400
        try:
            user.last_name = request.json['last_name']
        except KeyError:
            return jsonify(dict(errors=dict(
                                        message="No last_name provided."))), 400
        try:
            # Here we are encrypting the user's password using passlib. This way,
            # the user's password isn't in plaintext in the database.
            user.password = pwd_crypt_context.encrypt(request.json['password'])
        except KeyError:
            return jsonify(dict(errors=dict(message="No password provided."))), 400

        users.append(user)

    # Add the users
    try:
        with transaction(db_session):
            for user in users:
                db_session.add(user)
    except:
        return jsonify(dict(errors=dict(
                    message="Oops! There was an error adding the user."))), 500

    return jsonify(user.to_dict())


@app.route('/files', methods=['POST'])
@login_required
def files_user():
    """Upload a new file for the user.

    This just takes all files that were uplaoded (and no other form data) and
    tries to uplad them. This, like all other POST methods is an all or nothing
    deal such that if there is a single error, the entire upload is cancelled,
    and all files are deleted from the server.
    """
    uploaded_files = []
    try:
        for f in request.files:

            # Create a new random name to ensure no conflicts but keep the file
            # extension the same so that we at know what kind of file it is
            try:
                ext = os.path.splitext(secure_filename(
                                            request.files[f].filename))[1]
                new_file_name = '{}{}'.format(rand_str(10), ext)
            except:
                raise RuntimeError("Could not generate new name")

            # Save the file. Here we are making sure that each user has a
            # seperate file directory. In a real application, you'll want to
            # further subdivide this directory so that not all of your users
            # are in the same directory. I.e. something like
            #   uploads/
            #       1/
            #           <users 1-1000>
            #       2/
            #           <users 1001-200>
            try:
                file_name = user_uploaded_files.save(request.files[f],
                                    folder=str(current_user.id),
                                    name=new_file_name)
                file_path = user_uploaded_files.path(file_name)
            except UploadNotAllowed:
                raise ValueError("Upload not allowed")
            else:
                # Note how we're keeping the original file name as well. This
                # way, when the user downloads the file, it will have the
                # same name as when it was uploaded.
                uploaded_files.append(File(creator_id=current_user.id,
                                            file_name=request.files[f].filename,
                                            file_path=file_path))
    except:
        # Delete all of the file we just uploaded
        for f in uploaded_files:
            try:
                os.remove(f.file_path)
            except:
                pass
        return jsonify(dict(errors=dict(
                            message="Oops! Could not upload the files."))), 500
    else:
        try:
            # Try to add the files.
            with transaction(db_session):
                for uploaded_file in uploaded_files:
                    db_session.add(uploaded_file)
        except:
            # Delete all of the file we just uploaded
            for f in uploaded_files:
                try:
                    os.remove(f.file_path)
                except:
                    pass

            return jsonify(dict(errors=dict(
                        message="Oops! Could not save file metadata."))), 500
        else:
            return jsonify(dict(files=[f.to_dict() for f in uploaded_files]))


@app.route('/file/<file_id>', methods=['GET'])
@login_required
def get_file_for_user(file_id):
    """Get the contents of the specified file."""
    try:
        f = db_session.query(File).filter_by(id=file_id).one()
    except:
        return jsonify(dict(errors=dict(message="No such file"))), 404

    # Make sure this file belongs to the current user, otherwise, don't let him
    # see it.
    if f.creator_id != current_user.id:
        return jsonify(dict(errors=dict(
                    message="Nice try, but that's not your're file!"))), 403

    return send_file(os.path.join(f.file_path), as_attachment=True,
                    attachment_filename=f.file_name)


# =============================================================================
#
# Random helper functions
#
# =============================================================================


def rand_str(size=6, chars=string.ascii_uppercase + string.digits):
    """Create a random string."""
    return ''.join(random.choice(chars) for x in range(size))


# =============================================================================
#
# The main function to run the app
#
# =============================================================================


if __name__ == '__main__':

    # If we want to set things up, for the first time, make sure to do so
    if len(sys.argv) == 2 and sys.argv[1] == 'init':

        print "Initializing App..."

        # Create the directory or delete everything that is already there
        if os.path.exists(APP_DIR):
            shutil.rmtree(APP_DIR)
        os.makedirs(APP_DIR)

        # Initialize the database (i.e. create all of the necessary tables)
        initialize_db(engine)

        # Add an initial user
        db_session.add(User(first_name="Albert", last_name="Einstein",
                            email="albert@einstein.com",
                            password=pwd_crypt_context.encrypt("pass")))
        db_session.commit()

    # Start our app
    app.run()
